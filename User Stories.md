My Facebook app would have following features:

1. We can create our user account.
2. We can log in and log out from our account. 
3. We can send friend request to other people. 
4. Other people can send us friend request.
5. If we are friends with someone then they can able see our posts, photos and videos.
6. We can like our friends photos and videos.
7. We can upload a post which would be visible on our friends feed.
8. We can see our friends list.
9. We can delete our post.
10. We can search for new friends.



