## Database Schema Design  

1. Userauth table 
   * Userid (primary key)
   * Username
   * Password
   * First name
   * Last name
   * E mail

<br>

1. profile page 
   * Userid
   * bio
   * profile pic
   * friends

2. User friends
   * Friend userid (foreign key)
   * Userid (foreign key)
   * Friend Username
   * Status
   * Sentby

<br>

3. User post 
   * Postid (primary key)
   * Userid(foreign key)
   * Message
   * Created time

<br>

4. Comments
   * Userid(who commented) (foreign key of "Userauth")
   * Postid(foreign key of "User post" )
   * Comment
   * Comment time